module.exports = {
	'env': {
		'browser': true,
		'es6': true
	},
	'extends': [
		'eslint:recommended',
		'plugin:vue/recommended'
	],
	'globals': {
		'Atomics': 'readonly',
		'SharedArrayBuffer': 'readonly'
	},
	'parser': 'vue-eslint-parser',
	'parserOptions': {
		'parser': 'babel-eslint',
		'sourceType': 'module',
		'allowImportExportEverywhere': true
	},
	'plugins': [
		'vue'
	],
	'rules': {
		'indent': [
			'error',
			'tab'
		],
		'linebreak-style': [
			'error',
			'unix'
		],
		'quotes': [
			'error',
			'single'
		],
		'semi': [
			'error',
			'always'
		],
		'no-extra-parens': ['error', 'all'],
		'no-import-assign': 'error',
		'no-template-curly-in-string': 'error',
		'curly': ['error', 'multi-or-nest'],
		'dot-location': ['error', 'property'],
		'dot-notation': 'error',
		'eqeqeq': ['error', 'smart'],
		'vue/html-indent': [ 'error', 'tab' ],
		'vue/html-self-closing': [
			'error',
			{ 'html': { 'normal': 'never', 'void': 'always' } }
		],
		'vue/max-attributes-per-line': [
			'error',
			{ 'singleline': 3, 'multiline': 3 }
		],
		'vue/script-indent': [
			'error',
			'tab',
			{ 'baseIndent': 1, 'switchCase': 1 }
		]
	},
	'overrides': [
		// Need this override to use the vue/script-indent rule
		{
			'files': ['*.vue'],
			'rules': { 'indent': 'off' }
		}
	]
};
