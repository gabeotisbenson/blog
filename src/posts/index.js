const NewZealand = async () => await import('~/posts/NewZealand');

export default [
	{
		title: 'New Zealand',
		date: new Date('2020-02-01'),
		slug: 'new-zealand',
		component: NewZealand,
		snippet: 'I went to New Zealand in the summer of 2019 and it was unbelieveably great!'
	}
];
