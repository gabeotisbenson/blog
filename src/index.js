import 'core-js/stable';
import 'regenerator-runtime/runtime';
import '@openfonts/rubik_latin';
import '@openfonts/karla_latin';
import DefaultLayout from '~/components/layout/Default';
import router from '~/modules/router';
import Vue from 'vue';

Vue.filter('localize', date => date.toLocaleDateString(navigator.language || navigator.userLanguage,));

const app = new Vue({ ...DefaultLayout, router }).$mount('#default-layout');

export default app;
