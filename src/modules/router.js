import posts from '~/posts';
import Vue from 'vue';
import VueRouter from 'vue-router';

const Home = async () => await import('~/components/pages/Home');

Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	routes: [
		...posts.map(post => ({
			path: `/post/${post.slug}`, component: post.component
		})),
		{ path: '/', component: Home }
	]
});

export default router;
